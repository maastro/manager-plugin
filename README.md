# Manager Plugin

The Manager Plugin is an autoconfigured Spring Boot Plugin for the MIA Manager Service. It enables developers to build their own MIA Manager Service to manage container flows in the [MIA framework](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Home).

## Prerequisites ##

- Java 8
- 512 MB RAM
- Modern browser 

## Creating a custom Manager

To develop a custom manager, create a new spring boot project using spring initializr. Create a service, and give it an annotation with name: ```@Service("managerservice")```. Let this service implement 'ManagerService'. The overridden process method can be used to couple the available communication services such as the mapping service, container service and so on.
 
## Flow ##

For an example of a standard MIA workflow, see the [Universal Manager](https://bitbucket.org/maastrosdt/universalmanagerservice).
 
## Usage ##

 The [swagger interface](http://localhost:8200/swagger-ui.html) will provide a summary of all possible functions and requests.