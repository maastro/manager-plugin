package nl.maastro.mia.managerplugin.constants;

public final class DicomTag {
    
    public static final String MODALITY = "00080060";
    public static final String SERIES_INSTANCE_UID = "0020000E";

}
