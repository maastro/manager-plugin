package nl.maastro.mia.managerplugin.service;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import nl.maastro.mia.managerplugin.exception.MappingException;
import nl.maastro.mia.managerplugin.exception.MissingModalityException;

@Service 
public class ValidationService {
    
    public void validateStructureMappings(Map<String, String> mappings) throws MappingException {
        if (mappings.containsValue(null) || mappings.containsValue("")) {
            throw new MappingException("Not all required structures are mapped");
        }
    }
    
    public void validateRequiredModalities(Set<String> requiredModalities, 
            Set<String> availableModalities) throws MissingModalityException {

        Set<String> missingModalities = new HashSet<>();
        for (String requiredModality : requiredModalities) {
            if (!availableModalities.contains(requiredModality)) {
                missingModalities.add(requiredModality);
            }
        }
        if (!missingModalities.isEmpty()) {
            throw new MissingModalityException("Container is missing the following required modalities:" + missingModalities);
        }
    }
}
