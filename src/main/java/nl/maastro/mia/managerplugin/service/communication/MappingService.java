package nl.maastro.mia.managerplugin.service.communication;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriComponentsBuilder;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

import nl.maastro.mia.managerplugin.exception.MappingException;
import nl.maastro.mia.managerplugin.web.dto.RoiRtogDto;
import nl.maastro.mia.managerplugin.web.dto.configuration.VolumeOfInterestDto;
import nl.maastro.mia.managerplugin.web.dto.container.ContainerDto;

@Service
public class MappingService {

	private static final Logger logger = LoggerFactory.getLogger(MappingService.class);
	private static final String ROLE = "ROLE_USER";
	
	@Autowired
	RestTemplate restTemplate;

	@Value("${micro.mappingservice:mapper}")
	private String mappingServiceName;

	@Value("${spring.application.name:manager}")
	private String managerName;

	private EurekaClient eurekaClient;

	public MappingService(ContainerService containerService, EurekaClient eurekaClient) {
		this.eurekaClient = eurekaClient;
	}

	public RedirectView forwardToMappingService(ContainerDto container, Collection<VolumeOfInterestDto> volumesOfInterest){
		if (container == null){
			logger.error("Container DTO is empty, cannot proceed.");
			return badRequestView();
		}

		String mappingBaseUrl = this.getBaseUrlMapper();
		
		if (mappingBaseUrl == null){
			logger.error("Cannot find mappingService. MappingService name: " + mappingServiceName);
			return badRequestView();
		}
		
		ArrayList<String> rtogs = new ArrayList<>(this.getRtogs(volumesOfInterest));
		Collection<String> rois = container.getDicomPackage().getRoiNames();
		UriComponentsBuilder builder = ServletUriComponentsBuilder.fromUriString(mappingBaseUrl + "/api/mapping/mappingtool");
		
		builder.queryParam("containerId", container.getId().toString());
		builder.queryParam("userId", container.getUserId());
		builder.queryParam("userRole", ROLE);
		builder.queryParam("managerName", managerName);
		if (!rtogs.isEmpty())
			builder.queryParam("rtogs", this.listToUri(rtogs));
		if (!rois.isEmpty())
			builder.queryParam("rois", this.listToUri(rois));

		String url = builder.build().encode().toUriString();
		
		return new RedirectView(url);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" }) //W.r.t. Map<?,?> as this cannot be inferred by the Response Entity
	public Map<String,String> map(Set<String> rois, Set<VolumeOfInterestDto> vois, String userId) throws MappingException {
		RoiRtogDto roiRtogListDto = new RoiRtogDto();
		roiRtogListDto.setRoiList(rois);
		roiRtogListDto.setRtogList(getRtogs(vois));
		roiRtogListDto.setUserId(userId);
		String url = "http://" + mappingServiceName + "/api/mapping/getmappings";
	
		logger.info("Get mappings POST {} roiRtogListDto={}", url, roiRtogListDto);
        ResponseEntity<Map> response = restTemplate.postForEntity(url, roiRtogListDto, Map.class);

        if(response.getStatusCode().is2xxSuccessful()){
            response.getBody().forEach((key, value) -> logger.debug("RTOG {} mapped to {}", key, value));
            return response.getBody();
        } else {
            throw new MappingException("Unable to retrieve mappings for url: " + url);
        }		
	}
	
	private Set<String> getRtogs(Collection<VolumeOfInterestDto> voisDto){
		Set<String> rtogs = new HashSet<>();
		for(VolumeOfInterestDto dto : voisDto){
			rtogs.addAll(dto.getRtogs());
		}
		return rtogs;
	}
	
	private String listToUri(Collection<String> list){
		StringBuilder sb = new StringBuilder();
		for (String element : list){
			sb.append(element).append(",");
		}
		sb.setLength(Math.max(sb.length() - 1, 0));
		return sb.toString();
	}
	
	private String getBaseUrlMapper(){
		Application app = eurekaClient.getApplication(mappingServiceName);
		if (app == null){
			return null;
		}
		List<InstanceInfo> instances = app.getInstances();
		if (instances.isEmpty()){
			return null;
		}
		return instances.get(0).getHomePageUrl();
	}
	
	private RedirectView badRequestView(){
		RedirectView redirectView = new RedirectView();
		redirectView.setStatusCode(HttpStatus.BAD_REQUEST);
		return redirectView;
	}
}