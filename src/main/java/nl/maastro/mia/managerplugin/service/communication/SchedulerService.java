package nl.maastro.mia.managerplugin.service.communication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import nl.maastro.mia.managerplugin.web.dto.scheduling.TaskDto;

@Service
public class SchedulerService {
	
    private static final Logger logger = LoggerFactory.getLogger(SchedulerService.class);

	private RestTemplate restTemplate;

	@Value("${micro.schedulingservice:scheduler}")
	private String destination;
	
	@Value("${micro.calculationservice:openmdap}")
	private String calculationService;


	public SchedulerService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public void startTask(Long containerId, String userId, int priority) throws Exception {
	    
	    logger.info("Scheduling task for containerId: " + containerId);
		
		String url = "http://" + destination + "/api/task";
		TaskDto task = new TaskDto();
		task.setContainerId(containerId.toString());
		task.setUserId(userId);
		task.setCalculationService(calculationService);
		task.setPriority(priority);
		logger.info("POST {} task={}", url, task);
		try {
			restTemplate.postForEntity(url, task, String.class);
		} catch (Exception e){
			throw new Exception("Unable to schedule task", e);
		}
	}

}