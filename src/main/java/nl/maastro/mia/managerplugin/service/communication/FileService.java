package nl.maastro.mia.managerplugin.service.communication;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import nl.maastro.mia.managerplugin.constants.DicomTag;
import nl.maastro.mia.managerplugin.web.dto.DicomQueryDto;

@Service
public class FileService {
    
    private static final Logger logger = LoggerFactory.getLogger(FileService.class);
	
	private static final String HTTP = "http://";
	private static final String API_DICOM_TAGS = "/api/dicomtags";
	
	protected ObjectMapper objectMapper;
	protected RestTemplate restTemplate;
	protected String fileServiceName;
	    
	public FileService(ObjectMapper objectMapper, RestTemplate restTemplate, 
	        @Value("${micro.fileService:fileservice}") String fileServiceName) {
	    objectMapper.configure(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS, true);
	    this.objectMapper = objectMapper;
	    this.restTemplate = restTemplate;
	    this.fileServiceName = fileServiceName;
	}
	
	public String getSeriesInstanceUid(String sopInstanceUid) throws Exception {
	    return queryDicomAttribute(sopInstanceUid, DicomTag.SERIES_INSTANCE_UID, String.class);
	}
	
	public String getModality(String sopInstanceUid) throws Exception {
	    return queryDicomAttribute(sopInstanceUid, DicomTag.MODALITY, String.class);
    }
	
	/**
     * Generic method for querying single, non-nested DICOM attributes. For querying multiple 
     * and/or nested DICOM attributes, please refer to the 
     * {@link #queryDicomAttributes(String, List) queryDicomAttributes} method. 
     * 
     * @param sopInstanceUid SOP instance UID of the DICOM file to get the attribute from.
     * @param dicomTag The DICOM tag for the attribute that is to queried.
     * @param returnType The class type for the returned result (e.g. String).
     * @return the value of the DICOM attribute. 
     */
    protected <T> T queryDicomAttribute(String sopInstanceUid, 
            String dicomTag, Class<T> returnType) throws Exception {
        JsonNode queryResult = queryDicomAttributes(sopInstanceUid, 
                Collections.singletonList(Collections.singletonList(dicomTag)));
        return objectMapper.treeToValue(queryResult.get(dicomTag), returnType);
    }
	
	/**
     * Generic method for querying multiple (nested) DICOM attributes. Nested DICOM attributes
     * can be queried by providing a list of DICOM tags. The result is returned as a JsonNode, 
     * where the keys are set by the DICOM tags.   
     *  
     * @param sopInstanceUid SOP instance UID of the DICOM file to get the attribute from.
     * @param chainedDicomTags A list of chained DICOM tags that are to be queried.
     * @return query result in the form of a JSON.
     */
	protected JsonNode queryDicomAttributes(String sopInstanceUid, 
            List<List<String>> chainedDicomTags) throws Exception {
        DicomQueryDto dicomQuery = new DicomQueryDto();
        dicomQuery.setSopInstanceUid(sopInstanceUid);
        dicomQuery.setDicomTags(chainedDicomTags);
        String requestUrl = HTTP + fileServiceName + API_DICOM_TAGS;
        logger.debug("Query DICOM attributes POST={} body={}", requestUrl, dicomQuery);
        HttpEntity<DicomQueryDto> requestBody = new HttpEntity<>(dicomQuery);
        return restTemplate.postForObject(requestUrl, requestBody, JsonNode.class);
    }

}