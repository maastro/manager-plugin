package nl.maastro.mia.managerplugin.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class PluginService {
	private static final String MANAGERSERVICE = "managerservice";
	@Autowired
	private ApplicationContext context;

	public ManagerService getProcessingService(){
		Map<String, ManagerService> beansOfType = 
				context.getBeansOfType(ManagerService.class);
		if(beansOfType != null){
			return beansOfType.get(MANAGERSERVICE);
		}else{
			return null;
		}
	}
}
