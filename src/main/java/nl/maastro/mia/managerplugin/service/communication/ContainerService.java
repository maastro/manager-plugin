package nl.maastro.mia.managerplugin.service.communication;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import nl.maastro.mia.managerplugin.enumeration.ContainerStatus;
import nl.maastro.mia.managerplugin.web.dto.configuration.ConfigurationDto;
import nl.maastro.mia.managerplugin.web.dto.container.ContainerDto;
import nl.maastro.mia.managerplugin.web.dto.container.ContainerIdsDto;
import nl.maastro.mia.managerplugin.web.dto.container.DicomPackageDto;
import nl.maastro.mia.managerplugin.web.dto.container.MappingResultDto;

@Service
public class ContainerService {
    
    private static final Logger logger = LoggerFactory.getLogger(ContainerService.class);

	private static final String HTTP = "http://";
	private static final String CONTAINERAPI = "/api/container/";
	
	@Autowired
	RestTemplate restTemplate;
	@Value("${micro.containerservice:containerservice}")
	private String dicomContainerService;


	public ContainerDto createContainer(DicomPackageDto dicomPackage, String userId, String providerId, Integer inputPort) throws Exception {
    	String fromUrl = HTTP + dicomContainerService + "/api/container";

    	UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(fromUrl);
    	if(userId!=null)
    		builder.queryParam("userId", userId);
    	//Remove this once userIds are actually implemented, for now this is required to run any test
    	else
    		builder.queryParam("userId", "DEFAULT_USER_ID");
    	if(providerId!=null)
    		builder.queryParam("providerId", providerId);
    	if(inputPort!=null)
    	    builder.queryParam("inputPort", inputPort);
    	
    	URI url = builder.build().encode().toUri();
		logger.info("Post DicomPackage from FileService to ContainerService POST {}", url);
		ResponseEntity<?> response = restTemplate.postForEntity(url, dicomPackage, ContainerDto.class);
        if (response.getStatusCode().is2xxSuccessful()){
                URI containerLocation = response.getHeaders().getLocation();
                return getContainer(containerLocation);
        } else {
            throw new Exception("Failed to create container for dicomPackage:" + dicomPackage 
                    + ", userId:" + userId + ", providerId:" + providerId + ", inputPort:" + inputPort);
		}
	}
	
	public ContainerDto getContainer(URI url) throws Exception {
		logger.debug("Get container GET {}", url);
		ResponseEntity<ContainerDto> response = restTemplate.getForEntity(url, ContainerDto.class);
		if (response.getStatusCode().is2xxSuccessful()) {
			return response.getBody();
		} else {
			throw new Exception("Unable to get container GET {}" + url);
		}
	}
	
	public List<Long> getContainerIdsForUserId(URI url, MappingResultDto mappingResult){
		logger.debug("Get container ids POST {}", url);
		ResponseEntity<ContainerIdsDto> response = restTemplate.postForEntity(url, mappingResult, ContainerIdsDto.class);
		if(response.getStatusCode().is2xxSuccessful()){
			return response.getBody().getContainerIds();
		}
		logger.debug("Unable to get container ids using POST {}", url);
		return Collections.emptyList();
	}
	
	
	public boolean saveContainerMapping(Long containerId, Map<String,String> mapping){
		String url = HTTP + dicomContainerService + CONTAINERAPI + "mapping";
		try{
			logger.info("Update container mappings POST {}", url);
			MappingResultDto mappingResult = new MappingResultDto();
			mappingResult.setRtogRoiMap(mapping);
			mappingResult.setContainerId(containerId.toString());
			restTemplate.postForEntity(url, mappingResult, String.class);
			return true;
		}
		catch(RestClientException e){
			logger.error("Unable to update mapping", e);
			return false;
		}
	}
	
	public void deleteContainer(long containerId){
		
		String url = HTTP + dicomContainerService + CONTAINERAPI + containerId;
		
		try{
			logger.info("Deleting container DELETE {}", url);
			restTemplate.delete(url);
		}
		catch(Exception e){
			logger.error("Unable to DELETE container id=" + containerId, e);
		}
	}
	
    public void setContainerStatus(ContainerDto containerDto, ContainerStatus status){
        String url = HTTP + dicomContainerService + CONTAINERAPI+containerDto.getId()+"/status";
        logger.info("Set ContainerStatus={} PUT {}",status, url);
        restTemplate.put(url, status.toString());
    }

	
	public void saveContainerConfiguration(Long containerId, ConfigurationDto dto)  {
		String url = HTTP + dicomContainerService + CONTAINERAPI+containerId+"/configuration";
		logger.info("Save configuration to container PUT {} ", url);
		restTemplate.put(url, dto);
	}
	
	public void saveContainerConfigurationId(Long containerId, Long configurationId) {
		String url = HTTP + dicomContainerService + CONTAINERAPI+containerId+"/configurationid?configurationId="+configurationId;
		logger.info("Save configurationId to container PUT {}", url);
		restTemplate.put(url,null);
	}
	
	public URI getContainerLocation(Long id){
		URI uri = null;
		try {
			uri = new URI(HTTP+dicomContainerService+CONTAINERAPI+id);
		} catch (URISyntaxException e) {
			logger.error("URISyntaxException", e);
		}
		return uri;
	}
	
	public URI getContainerLocationByUserId(){
		URI uri = null;
		try {
			uri = new URI(HTTP+dicomContainerService+"/api/container/user");
		} catch (URISyntaxException e) {
			logger.error("URISyntaxException", e);
		}
		return uri;
	}
	
}