package nl.maastro.mia.managerplugin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.maastro.mia.managerplugin.exception.*;
import nl.maastro.mia.managerplugin.web.dto.container.DicomPackageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import nl.maastro.mia.managerplugin.enumeration.ContainerStatus;
import nl.maastro.mia.managerplugin.service.communication.ContainerService;
import nl.maastro.mia.managerplugin.service.communication.MappingService;
import nl.maastro.mia.managerplugin.service.communication.SchedulerService;
import nl.maastro.mia.managerplugin.web.dto.configuration.ConfigurationDto;
import nl.maastro.mia.managerplugin.web.dto.configuration.VolumeOfInterestDto;
import nl.maastro.mia.managerplugin.web.dto.container.ContainerDto;
import nl.maastro.mia.managerplugin.web.dto.container.DicomPackagesDto;

public abstract class ManagerService {
    
    private static final Logger logger = LoggerFactory.getLogger(ManagerService.class);
    
    protected ContainerService containerService;
    protected MappingService mappingService;
    protected ValidationService validationService;
    protected SchedulerService schedulerService;
    
    public ManagerService(
            ContainerService containerService,
            MappingService mappingService,
            ValidationService validationService,
            SchedulerService schedulerService) {
        
        this.containerService = containerService;
        this.mappingService = mappingService;
        this.validationService = validationService;
        this.schedulerService = schedulerService;
    }
    
    protected abstract List<ContainerDto> createContainers(DicomPackagesDto dicomPackages, String providerId, Integer inputPort) throws Exception;
    
    protected abstract ConfigurationDto getConfiguration(ContainerDto container) throws Exception;
    
    protected abstract void validateRequiredModalities(ContainerDto container) throws MissingModalityException;

    protected abstract void postValidate(ContainerDto container) throws PostValidationException;

    protected abstract Integer inferPriority(ContainerDto container);

    @Async
    public void process(DicomPackagesDto dicomPackages, 
            String providerId,
            Integer inputPort,
            Map<String, String> mappings) throws Exception {

        List<DicomPackageDto> packages = dicomPackages.getDicomPackages();
        if( packages == null ||
                packages.isEmpty() ||
                packages.get(0).getTriggeringSopInstanceUids() == null ||
                packages.get(0).getTriggeringSopInstanceUids().isEmpty()){
            logger.warn("--- EMPTY TRIGGER ---");
            return;
        }
        List<String> triggeringUids = packages.get(0).getTriggeringSopInstanceUids();
        logger.info("--- STEP 1. CREATE CONTAINERS FOR TRIGGERING UIDS {} ---", triggeringUids);
        List<ContainerDto> containers = createContainers(dicomPackages, providerId, inputPort);
        logger.info("--- STEP 1. CREATING CONTAINERS FINISHED; {} CONTAINERS CREATED ---", containers.size());
        logger.info("--- STEP 2. PROCESSING CONTAINERS FOR TRIGGERING UIDS {}", triggeringUids);
        for(int i=0; i<containers.size(); i++){
            ContainerDto container = containers.get(i);
            logger.info("--- START PROCESSING CONTAINER ({}/{}); CONTAINER_ID={} {} ---", i+1, containers.size(), container.getId(), container);
            processContainer(container, mappings);
            logger.info("--- FINISHED PROCESSING CONTAINER ({}/{}); CONTAINER_ID={} {} ---", i+1, containers.size(), container.getId(), container);
        }
        logger.info("--- STEP 2. FINISHED PROCESSING CONTAINERS FOR TRIGGERING UIDS {}", triggeringUids);
    }
    
    public void processContainer(ContainerDto container, Map<String, String> priorMappings) {
        try {
            container = setConfiguration(container);
            container = setStructureMappings(container, priorMappings);
            validateRequiredModalities(container);
            postValidate(container);
            Integer priority = inferPriority(container);
            schedulerService.startTask(container.getId(), container.getUserId(), priority);
            containerService.setContainerStatus(container, ContainerStatus.QUEUE);
            logger.info("Container added to queue; containerId={}", container.getId());

        } catch(ConfigurationException e){
            logger.error("Configuration error while proccessing container", e);
            containerService.setContainerStatus(container, ContainerStatus.CONFIGURATIONERROR);
            
        } catch(MappingException e){
            logger.error("Mapping error while proccessing container", e);
            containerService.setContainerStatus(container, ContainerStatus.MAPPINGERROR);
            
        } catch(MissingModalityException e){
            logger.error("Validation error while processing container; deleting container for id=" + container.getId(), e);
            containerService.deleteContainer(container.getId());

        } catch(PostValidationException e){
            logger.warn("PostValidationException for container=" + container.getId() + " " + e.getMessage());
            containerService.deleteContainer(container.getId());
        }
        catch (Exception e) {
            logger.error("Exception while processing container", e);
            containerService.setContainerStatus(container, ContainerStatus.IDLE);
        }
    }
    
    private ContainerDto setConfiguration(ContainerDto container) throws Exception {
        logger.info("Setting configuration for container id={}", container.getId());
        ConfigurationDto configuration = getConfiguration(container);
        container.setConfiguration(configuration);
        container.setConfigurationId(configuration.getId());
        containerService.saveContainerConfiguration(container.getId(), configuration);
        containerService.saveContainerConfigurationId(container.getId(), configuration.getId());
        return container;
    }
        
    private ContainerDto setStructureMappings(ContainerDto container, Map<String, String> priorMappings) throws MappingException {
        logger.info("Setting structure mappings for containerId={}", container.getId());
        Map<String, String> mappings = new HashMap<>();
        Set<VolumeOfInterestDto> volumesOfInterest = container.getConfiguration().getVolumesOfInterest();
        if (volumesOfInterest != null && !volumesOfInterest.isEmpty()) {
            if (priorMappings != null && !priorMappings.isEmpty()){
                mappings = priorMappings;
            } else {
                Set<String> structureNames = container.getDicomPackage().getRoiNames();
                mappings = mappingService.map(structureNames, volumesOfInterest, container.getUserId());
            }
        }
        validationService.validateStructureMappings(mappings);
        container.setMappingRtogRoi(mappings);
        containerService.saveContainerMapping(container.getId(), mappings);
        return container;
    }
}

