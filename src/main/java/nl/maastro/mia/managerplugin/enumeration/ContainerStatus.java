package nl.maastro.mia.managerplugin.enumeration;

/**
 * The CalculationStatus enumeration.
 */
public enum ContainerStatus {
    IDLE,PREPROCESSING,QUEUE,RUNNING,DONE,
    CONFIGURATIONERROR,MAPPINGERROR,VALIDATIONERROR,CALCULATIONERROR,EXPORTERROR
}
