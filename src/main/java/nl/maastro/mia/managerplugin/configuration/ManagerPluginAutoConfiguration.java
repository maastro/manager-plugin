package nl.maastro.mia.managerplugin.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@ComponentScan("nl.maastro.mia.managerplugin")
@EnableAsync
public class ManagerPluginAutoConfiguration {
}