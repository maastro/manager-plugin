package nl.maastro.mia.managerplugin.exception;

public class MappingException extends Exception {

    public MappingException(String message) {
        super(message);
    }
}
