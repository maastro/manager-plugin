package nl.maastro.mia.managerplugin.exception;

public class PostValidationException extends Exception {

    public PostValidationException(String message) {
        super(message);
    }
    public PostValidationException(String message, Throwable cause) {
        super(message, cause);
    }

}
