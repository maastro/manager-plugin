package nl.maastro.mia.managerplugin.exception;

public class MissingModalityException extends Exception {

    public MissingModalityException(String message) {
        super(message);
    }
    public MissingModalityException(String message, Throwable cause) {
        super(message, cause);
    }

}
