package nl.maastro.mia.managerplugin.web.dto.container;

import java.util.Date;

public class ContainerLogDto {

	//Default constructor necessary for dto
	public ContainerLogDto() {}
	
	public ContainerLogDto(String level, long containerId, String doseFraction, String ctFraction, String status, 
			String serviceName, String messageUuid, String message, Date entryDate){
		
		this.level = level;
		this.containerId = containerId;
		this.doseFraction = doseFraction;
		this.ctFraction = ctFraction;
		this.status = status;
		this.serviceName = serviceName;
		this.messageUuid = messageUuid;
		this.message = message;
		this.entryDate = entryDate;
	}
	
	private String level;
	private long containerId;
	private String doseFraction;
	private String ctFraction;
	private String status;
	private String serviceName;
	private String messageUuid;
	private String message;
	private Date entryDate;
	
	
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public long getContainerId() {
		return containerId;
	}
	public void setContainerId(long containerId) {
		this.containerId = containerId;
	}
	public String getDoseFraction() {
		return doseFraction;
	}
	public void setDoseFraction(String doseFraction) {
		this.doseFraction = doseFraction;
	}
	public String getCtFraction() {
		return ctFraction;
	}
	public void setCtFraction(String ctFraction) {
		this.ctFraction = ctFraction;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getMessageUuid() {
		return messageUuid;
	}
	public void setMessageUuid(String messageUuid) {
		this.messageUuid = messageUuid;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}
}
