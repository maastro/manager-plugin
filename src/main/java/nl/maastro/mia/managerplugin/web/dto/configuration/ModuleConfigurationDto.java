package nl.maastro.mia.managerplugin.web.dto.configuration;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ModuleConfigurationDto {
	private String moduleName;
	private List<ComputationDto> computations;
	private Set<String> requiredModalities = new HashSet<>();

	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String identifier) {
		this.moduleName = identifier;
	}
	public List<ComputationDto> getComputations() {
		return computations;
	}
	public void setComputations(List<ComputationDto> calculations) {
		this.computations = calculations;
	}
	public Set<String> getRequiredModalities() {
		return requiredModalities;
	}
	public void setRequiredModalities(Set<String> requiredModalities) {
		this.requiredModalities = requiredModalities;
	}
}