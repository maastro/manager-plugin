package nl.maastro.mia.managerplugin.web.dto.scheduling;

import javax.validation.constraints.NotNull;

public class TaskDto {
	
	@NotNull
	private String containerId;
				
	@NotNull
	private String calculationService;
	
	private String userId;
	
	int priority = 100;
	
	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getCalculationService() {
		return calculationService;
	}

	public void setCalculationService(String calculationService) {
		this.calculationService = calculationService;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getPriority() {
		return priority;
	}

	@Override
	public String toString() {
		return "TaskDto{" +
				"containerId='" + containerId + '\'' +
				", calculationService='" + calculationService + '\'' +
				", userId='" + userId + '\'' +
				", priority=" + priority +
				'}';
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}


}
