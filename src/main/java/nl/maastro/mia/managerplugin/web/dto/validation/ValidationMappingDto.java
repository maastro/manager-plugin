package nl.maastro.mia.managerplugin.web.dto.validation;

import java.util.Map;

public class ValidationMappingDto {

	private Map<String, String> mappings;
	
	public Map<String, String> getMappings() {
		return mappings;
	}
	public void setMappings(Map<String, String> mappings) {
		this.mappings = mappings;
	}
}
