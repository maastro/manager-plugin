package nl.maastro.mia.managerplugin.web.controller;

import java.net.URI;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import nl.maastro.mia.managerplugin.exception.ConfigurationException;
import nl.maastro.mia.managerplugin.service.ManagerService;
import nl.maastro.mia.managerplugin.service.PluginService;
import nl.maastro.mia.managerplugin.service.communication.ContainerService;
import nl.maastro.mia.managerplugin.service.communication.MappingService;
import nl.maastro.mia.managerplugin.web.dto.configuration.VolumeOfInterestDto;
import nl.maastro.mia.managerplugin.web.dto.container.ContainerDto;
import nl.maastro.mia.managerplugin.web.dto.container.MappingResultDto;


@RestController
@RequestMapping("/api")
public class MappingController {
    
    private static final Logger logger = LoggerFactory.getLogger(MappingController.class);
	
	@Autowired
	PluginService pluginService;
	@Autowired
	private MappingService mappingService;
	@Autowired
	private ContainerService containerService;
		
	@Value("${micro.containerservice:containerservice}")
	private String dicomContainerService;
	
	
	@RequestMapping(value = "/map/container/{id}", method = RequestMethod.GET)
	public RedirectView forwardToMappingService(@PathVariable Long id) throws ConfigurationException {
		
		URI containerLocation = containerService.getContainerLocation(id);
		ContainerDto containerDto;
		try {
			containerDto = containerService.getContainer(containerLocation);
		} catch (Exception e) {
			logger.error("Container DTO is empty, cannot proceed", e);
			RedirectView redirectView = new RedirectView();
			redirectView.setStatusCode(HttpStatus.BAD_REQUEST);
			return redirectView;
		}

		Collection<VolumeOfInterestDto> volumesOfInterest = containerDto.getConfiguration().getVolumesOfInterest();
		return mappingService.forwardToMappingService(containerDto, volumesOfInterest);
	}
	
	@RequestMapping(value = "/map/container", method = RequestMethod.POST)
	public ResponseEntity<?> updatedMapping(@RequestBody MappingResultDto mappingResult) throws Exception {
		
		ManagerService processingService = pluginService.getProcessingService();
		if(processingService == null){
			return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		URI currentContainerLocation = containerService.getContainerLocation(Long.parseLong(mappingResult.getContainerId()));
		ContainerDto currentContainer = containerService.getContainer(currentContainerLocation);
		processingService.processContainer(currentContainer, mappingResult.getRtogRoiMap());
		
		URI containerLocationsForUser = containerService.getContainerLocationByUserId();
		List<Long> containerIdsForUser = containerService.getContainerIdsForUserId(containerLocationsForUser, mappingResult);
		
		containerIdsForUser.remove(Long.parseLong(mappingResult.getContainerId()));
		logger.info("In addition to container " + mappingResult.getContainerId() + ", also updating containers: " + containerIdsForUser);
		
		for (Long id : containerIdsForUser){
			URI containerLocation = containerService.getContainerLocation(id);
			ContainerDto container = containerService.getContainer(containerLocation);
			processingService.processContainer(container, null);
		}
		
		return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.CREATED);
	}
}
