package nl.maastro.mia.managerplugin.web.dto.configuration;

import java.util.List;

public class VolumeOfInterestDto {
	String name;
	List<String> rtogs;
	List<String> operators;
	
	public List<String> getRtogs() {
		return rtogs;
	}
	
	public void setRtogs(List<String> rtog) {
		this.rtogs = rtog;
	}
	
	public List<String> getOperators() {
		return operators;
	}
	
	public void setOperators(List<String> operators) {
		this.operators = operators;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}