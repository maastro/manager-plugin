package nl.maastro.mia.managerplugin.web.dto.container;

import java.util.Objects;

public class ImageDto {
	private String location;
	private String sopUid;
	
	public ImageDto(){ }
	
	public ImageDto(String location, String sopUid){
		
		this.location = location;
		this.sopUid = sopUid;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getSopUid() {
		return sopUid;
	}
	public void setSopUid(String sopUid) {
		this.sopUid = sopUid;
	}

	@Override
	public String toString() {
		return "ImageDto{" +
				"location='" + location + '\'' +
				", sopUid='" + sopUid + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ImageDto imageDto = (ImageDto) o;
		return Objects.equals(sopUid, imageDto.sopUid);
	}

	@Override
	public int hashCode() {
		return Objects.hash(sopUid);
	}
}