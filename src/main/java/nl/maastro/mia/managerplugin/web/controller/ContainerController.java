package nl.maastro.mia.managerplugin.web.controller;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.managerplugin.service.ManagerService;
import nl.maastro.mia.managerplugin.service.PluginService;
import nl.maastro.mia.managerplugin.service.communication.ContainerService;
import nl.maastro.mia.managerplugin.web.dto.container.ContainerDto;
import nl.maastro.mia.managerplugin.web.dto.container.DicomPackagesDto;

@RestController
@RequestMapping("/api")
public class ContainerController {
    
    private static final Logger logger = LoggerFactory.getLogger(ContainerController.class);
	
	@Autowired
	ContainerService containerService;

	@Autowired
	PluginService pluginService;


	@RequestMapping(value="/container", method=RequestMethod.POST)
	public ResponseEntity<?> createContainer(
			@RequestBody DicomPackagesDto dicomPackages,
			@RequestParam(required=false) String providerId,
			@RequestParam(required=false) String configurationId
			){
		Integer inputPort;
		try{
			inputPort = Integer.parseInt(configurationId);
		}catch(NumberFormatException e){
			logger.error("Unable to parse integer from configurationId " + configurationId);
			return ResponseEntity.badRequest().build();
		}
		
		ManagerService processingService = pluginService.getProcessingService();
		try {
			processingService.process(dicomPackages, providerId, inputPort, null);
			return ResponseEntity.ok().build();			
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@RequestMapping(value="/container/manual/{containerId}", method=RequestMethod.GET)
	public boolean manual(@PathVariable Long containerId){
		URI url = containerService.getContainerLocation(containerId);
		try {
			ContainerDto container = containerService.getContainer(url);
			ManagerService processingService = pluginService.getProcessingService();
			processingService.processContainer(container, null);
        } catch (Exception e) {
            logger.warn("Could not find container for id:" + containerId, e);
            return false;
        }
        return true;
	}
}