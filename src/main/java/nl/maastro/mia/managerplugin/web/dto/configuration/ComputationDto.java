package nl.maastro.mia.managerplugin.web.dto.configuration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComputationDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private String identifier;
	private List<String> volumeOfInterestNames;
	private String configuration;
	private String moduleName;
	private Map<String, String> inputModalities = new HashMap<>(); 
	private List<String> outputModalities = new ArrayList<>(); 
	
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getConfiguration() {
		return configuration;
	}
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}
	public List<String> getVolumeOfInterestNames() {
		return volumeOfInterestNames;
	}
	public void setVolumeOfInterestNames(List<String> volumeOfInterestNames) {
		this.volumeOfInterestNames = volumeOfInterestNames;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	@Override
	public String toString() {
		return "ComputationDto [identifier=" + identifier + ", configuration=" + configuration
				+ ", volumeOfInterestName=" + volumeOfInterestNames + "]";
	}

	public Map<String, String> getInputModalities() {
		return inputModalities;
	}
	public void setInputModalities(Map<String, String> inputModalities) {
		this.inputModalities = inputModalities;
	}
	public List<String> getOutputModalities() {
		return outputModalities;
	}
	public void setOutputModalities(List<String> outputModalities) {
		this.outputModalities = outputModalities;
	}
}