package nl.maastro.mia.managerplugin.web.dto.configuration;

import java.util.List;
import java.util.Set;

public class ConfigurationDto {
    
    private Long id;
	private String name;
	private Integer outputPort;
	private String initializingServiceName;
	private Set<VolumeOfInterestDto> volumesOfInterest;
	private List<ModuleConfigurationDto> modulesConfiguration;
    
	public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Integer getOutputPort() {
        return outputPort;
    }
    
    public void setOutputPort(Integer outputPort) {
        this.outputPort = outputPort;
    }
    
    public String getInitializingServiceName() {
        return initializingServiceName;
    }
    
    public void setInitializingServiceName(String initializingServiceName) {
        this.initializingServiceName = initializingServiceName;
    }
    
    public Set<VolumeOfInterestDto> getVolumesOfInterest() {
        return volumesOfInterest;
    }
    
    public void setVolumesOfInterest(Set<VolumeOfInterestDto> volumesOfInterest) {
        this.volumesOfInterest = volumesOfInterest;
    }
    
    public List<ModuleConfigurationDto> getModulesConfiguration() {
        return modulesConfiguration;
    }
    
    public void setModulesConfiguration(List<ModuleConfigurationDto> modulesConfiguration) {
        this.modulesConfiguration = modulesConfiguration;
    }
}
