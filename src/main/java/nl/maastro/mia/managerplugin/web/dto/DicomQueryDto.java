package nl.maastro.mia.managerplugin.web.dto;

import java.util.List;

public class DicomQueryDto {
    
    private String sopInstanceUid;
    
    private List<List<String>> dicomTags;

    public String getSopInstanceUid() {
        return sopInstanceUid;
    }

    public void setSopInstanceUid(String sopInstanceUid) {
        this.sopInstanceUid = sopInstanceUid;
    }

    public List<List<String>> getDicomTags() {
        return dicomTags;
    }

    public void setDicomTags(List<List<String>> dicomTags) {
        this.dicomTags = dicomTags;
    }

    @Override
    public String toString() {
        return "DicomQueryDto [sopInstanceUid=" + sopInstanceUid + ", dicomTags=" + dicomTags + "]";
    }

}
