package nl.maastro.mia.managerplugin.web.dto.container;

import java.util.ArrayList;
import java.util.List;

public class ContainerIdsDto {

	private List<Long> containerIds = new ArrayList<>();

	public List<Long> getContainerIds() {
		return containerIds;
	}

	public void setContainerIds(List<Long> containerIds) {
		this.containerIds = containerIds;
	}
}
