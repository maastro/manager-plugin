package nl.maastro.mia.managerplugin.web.dto.validation;

import java.util.Set;

public class ValidationModalityDto {

	private Set<String> requiredModalityList;
	private Set<String> modalityList;
	
	
	public Set<String> getRequiredModalityList() {
		return requiredModalityList;
	}
	public void setRequiredModalityList(Set<String> requiredModalityList) {
		this.requiredModalityList = requiredModalityList;
	}
	public Set<String> getModalityList() {
		return modalityList;
	}
	public void setModalityList(Set<String> modalityList) {
		this.modalityList = modalityList;
	}
}
