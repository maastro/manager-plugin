package nl.maastro.mia.managerplugin.web.dto.container;

import java.util.HashMap;
import java.util.Map;

import nl.maastro.mia.managerplugin.web.dto.configuration.ConfigurationDto;

public class ContainerDto {
	
	private String containerStatus;
		
	private Map<String,String> mappingRtogRoi = new HashMap<>();
	
	private DicomPackageDto dicomPackage;
	
	private ConfigurationDto configuration;
	
	private String userId;
	
	private Integer inputPort;
	
	private Long id;
	
	private String calculationId;
	
	private Long configurationId;

	public String getCalculationId() {
		return calculationId;
	}
	
	public void setCalculationId(String calculationId) {
		this.calculationId = calculationId;
	}
	
	public DicomPackageDto getDicomPackage() {
		return dicomPackage;
	}
	
	public void setDicomPackage(DicomPackageDto dicomPackage) {
		this.dicomPackage = dicomPackage;
	}
	
	public Map<String, String> getMappingRtogRoi() {
		return mappingRtogRoi;
	}
	
	public void setMappingRtogRoi(Map<String, String> mappingRtogRoi) {
		this.mappingRtogRoi = mappingRtogRoi;
	}
	
	public String getContainerStatus() {
		return containerStatus;
	}

	public void setContainerStatus(String containerStatus) {
		this.containerStatus = containerStatus;
	}

	public ConfigurationDto getConfiguration() {
		return configuration;
	}

	public void setConfiguration(ConfigurationDto configuration) {
		this.configuration = configuration;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getInputPort() {
		return inputPort;
	}

	public void setInputPort(Integer inputPort) {
		this.inputPort = inputPort;
	}

	public Long getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(Long configurationId) {
		this.configurationId = configurationId;
	}

	@Override
	public String toString() {
		return "ContainerDto{" +
				"containerStatus='" + containerStatus + '\'' +
				", mappingRtogRoi=" + mappingRtogRoi +
				", configuration=" + configuration +
				", inputPort=" + inputPort +
				", calculationId='" + calculationId + '\'' +
				", configurationId=" + configurationId +
				'}';
	}
}