package nl.maastro.mia.managerplugin.web.dto.container;

import java.util.List;

public class DicomPackagesDto {
	private List<DicomPackageDto> dicomPackages;

	public List<DicomPackageDto> getDicomPackages() {
		return dicomPackages;
	}

	public void setDicomPackages(List<DicomPackageDto> dicomPackages) {
		this.dicomPackages = dicomPackages;
	}
}
