package nl.maastro.mia.managerplugin.web.dto.validation;

public class ValidationResultDto {

	
	private boolean validationPassed;
	private String errorMessage;
	
	
	public boolean isValidationPassed() {
		return validationPassed;
	}
	public void setValidationPassed(boolean validationPassed) {
		this.validationPassed = validationPassed;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
