package nl.maastro.mia.managerplugin.web.dto;

import java.util.Set;

public class RoiRtogDto {

	private Set<String> roiList;
	private Set<String> rtogList;
	private String userId;
	
	public Set<String> getRoiList() {
		return roiList;
	}
	public void setRoiList(Set<String> roiList) {
		this.roiList = roiList;
	}
	public Set<String> getRtogList() {
		return rtogList;
	}
	public void setRtogList(Set<String> rtogList) {
		this.rtogList = rtogList;
	}
	@Override
	public String toString() {
		return "RoiRtogDto [roiList=" + roiList + ", rtogList=" + rtogList + "]";
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
