package nl.maastro.mia.managerplugin.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import nl.maastro.mia.managerplugin.exception.MappingException;
import nl.maastro.mia.managerplugin.exception.MissingModalityException;

public class ValidationServiceTests {
    
    private ValidationService validationService = new ValidationService();
    
    @Test
    public void testValidateStructureMappings() throws MappingException {
        Map<String, String> mappings = new HashMap<>();
        mappings.put("GTVp", "GTVp1");
        mappings.put("Eso", "Esophagus");
        mappings.put("LuNG-1", "Linkerlong"); //check case insensitivity
        validationService.validateStructureMappings(mappings);
    }
        
    @Test(expected = MappingException.class)
    public void testValidateStructureMappingsForEmptyRoi() throws MappingException {
        Map<String, String> mappings = new HashMap<>();
        mappings.put("GTVp", "GtV-1");//check case insensitivity
        mappings.put("Eso", "Esophagus");
        mappings.put("SomeRTOG", "");
        validationService.validateStructureMappings(mappings);
    }
    
    @Test(expected = MappingException.class)
    public void testValidateStructureMappingsForNullRoi() throws MappingException {
        Map<String, String> mappings = new HashMap<>();
        mappings.put("GTVp", "GtV-1");//check case insensitivity
        mappings.put("Eso", "Esophagus");
        mappings.put("SomeRTOG", null);
        validationService.validateStructureMappings(mappings);
    }
    
    @Test
    public void testValidateRequiredModalities() throws MissingModalityException {
        Set<String> requiredModalities = new HashSet<>();
        requiredModalities.add("CT");
        requiredModalities.add("RTSTRUCT");
        
        Set<String> availableModalities = new HashSet<>();
        availableModalities.add("CT");
        availableModalities.add("RTSTRUCT");
        availableModalities.add("");
        availableModalities.add("RTDOSE");
        
        validationService.validateRequiredModalities(requiredModalities, availableModalities);
    }
    
    @Test(expected = MissingModalityException.class)
    public void testValidateRequiredModalitiesForMissingRtStruct() throws MissingModalityException {
        Set<String> requiredModalities = new HashSet<>();
        requiredModalities.add("CT");
        requiredModalities.add("RTSTRUCT");
        
        Set<String> availableModalities = new HashSet<>();
        availableModalities.add("CT");
        availableModalities.add("");
        availableModalities.add("RTDOSE");
        
        validationService.validateRequiredModalities(requiredModalities, availableModalities);
    }
    
}
